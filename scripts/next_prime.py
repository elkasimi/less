def is_prime(n):
    for i in xrange(2, n):
        if n%i == 0: return False
        if i*i > n: break
    
    return True

n = input()
p = n+1
while not is_prime(p):
    p += 1
    
print 'next prime to', n, 'is', p
