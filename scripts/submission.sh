#! /bin/sh

cat ./player/Common.h \
    ./player/RandomNumberGenerator.h \
    ./player/Timer.h \
    ./player/TranspositionTable.h \
    ./player/Board.h \
    ./player/Strategy.h \
    ./player/Conversion.h \
    ./player/ExpectMinMaxStrategy.h \
    ./player/RandomStrategy.h \
    ./player/RunStrategy.h \
    ./player/Node.h \
    ./player/MCTSStrategy.h \
    ./player/RandomNumberGenerator.cc \
    ./player/Timer.cc \
    ./player/TranspositionTable.cc \
    ./player/Board.cc \
    ./player/ExpectMinMaxStrategy.cc \
    ./player/RandomStrategy.cc \
    ./player/RunStrategy.cc \
    ./player/Conversion.cc \
    ./player/Node.cc \
    ./player/MCTSStrategy.cc \
    ./player/main.cc > ./player/Submission.cc
sed -i '/#include "/d' ./player/Submission.cc
sed -i '/#pragma once/d' ./player/Submission.cc

