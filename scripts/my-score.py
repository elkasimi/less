import requests
from operator import itemgetter
#urls = [u'http://www.codecup.nl/competitionround.php?cr=3398',
#        u'http://www.codecup.nl/competitionround.php?cr=3399',
#        u'http://www.codecup.nl/competitionround.php?cr=3400',
#        u'http://www.codecup.nl/competitionround.php?cr=3401']
urls = [u'http://www.codecup.nl/competitionround.php?cr=3423']

scores = {}

nGames = 0

values = {}
for url in urls:
    t = requests.get(url).text
    p = 0
    while True:
        p = t.find('showgame.php', p)
        if p < 0:
            break
        pl = []
        for i in range(4):
            p = t.find('>', t.find('<div', p)) + 1
            pl.append(t[p:t.find('<', p)])
        p = t.find('<td class=kimborder>', p)
        try:
            sc = [int(s.strip()) for s in t[p+20:t.find('<', p+20)].split('-')]
            for i in range(4):
                player = pl[i]
                score = sc[i]
                print 'p=', player, ' s=', score
                if player in values:
                    values[player][score] += 1
                else:
                    values[player] = [0] * 21
                    values[player][score] = 1          
                if player in scores:
                    scores[player] = (scores[player][0] + score, scores[player][1] + 1)                     
                else:
                    scores[player] = (score, 1)
            nGames += 1
        except:
            pass
                
# !!! http://stackoverflow.com/questions/1915564/python-convert-a-dictionary-to-a-sorted-list-by-value-instead-of-key
points = dict(scores)
for k in scores.keys():
    scores[k] = float(scores[k][0]) / scores[k][1]
sclist = sorted(scores.iteritems(), key=itemgetter(1), reverse=True)
for n in range(len(sclist)):
    sc = sclist[n]
    print '%d: %s\t\t%0.2f\t%s' % (n+1, sc[0], sc[1], str(points[sc[0]]))
print '#games = %d' % nGames
print values
